<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require('animal.php'); {
 
        $sheep = new Animal("shaun");
        echo "Name : ".$sheep->sheep."<br>";
        echo "Legs : ".$sheep->legs."<br>";
        echo "Cold blooded : ".$sheep->cold_blooded."<br>"; 
        echo "<br>";
        }

        require('frog.php'); {

        $kodok = new kodok("Name : Buduk", "Legs : 4", "Cold Blooded : No", "Jump : Hip hop");
        echo $kodok->get_buduk();
        echo "<br>";
        echo $kodok->get_legs();
        echo "<br>";
        echo $kodok->get_cold_blooded();
        echo "<br>";
        echo $kodok->get_jump();
        echo "<br>";
        echo "<br>";
        }

        require('ape.php'); {
        
            $sungokong = new Ape("Kera Sakti", "Legs : 2", "Clod Blooded : No", "Yell : Auooo");
            function __destruct() {
                echo "Name : {$this->sungokong}<br>{$this->legs}<br>{$this->cold_blooded}<br>{$this->yell}";
        }
             
    }
    ?>
</body>
</html>