<?php

class ape {
    public $sungokong;
    public $legs = 2;
    public $cold_blooded = "no";
    public $yell = "Auoooo";

    function __construct($sungokong, $legs, $cold_blooded, $yell) {
        $this->sungokong = $sungokong;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
        $this->yell = $yell;
    }
    function __destruct() {
        echo "Name : {$this->sungokong}<br>{$this->legs}<br>{$this->cold_blooded}<br>{$this->yell}";
    }
}
?>