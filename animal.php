<!DOCTYPE html>
<html>
<body>

<?php

class animal {
    public $sheep;
    public $legs = 4;
    public $cold_blooded = 'no';

    public function __construct($string){
        $this->sheep = $string;
    }
}

   // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>
</html>
</body>